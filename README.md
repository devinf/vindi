# Vindi

## Inicialização

token: obrigatório

items_per_page: 25 padrão

timeout: 30 padrão

```python
>>> from vindi import Vindi
>>> vindi = Vindi(token='API_KEY', items_per_page=25, timeout=30)
```

## Queries

[Documentação da Vindi](https://atendimento.vindi.com.br/hc/pt-br/articles/204163150)

## Customers

### get by id

```python
>>> customer = Customer.get(2714152)
>>> customer
Cliente1
```

### get list
Por padrão este método não traz os usuarios arquivados: `'query=-status:archived'`

```python
>>> customer = Customer.get_list(query='name:Cliente')
>>> customer
[Cliente1, Cliente2, Cliente_n]
```

### create new

```python
>>> customer = Customer(name='Cliente Teste')
>>> customer.save()
>>> customer
Cliente1
```

### update

```python
>>> customer = Customer.get(2714152)
>>> customer.name = 'Cliente1'
>>> customer.save()
>>> customer
Cliente1
```

### remove
Os usuários removidos são arquivados, só podem ser excluídos completamente pelo sistema

```python
>>> customer = Customer.get(2714152)
>>> customer.remove()
>>> customer
Cliente1 - archived
```


## PaymentProfileCreditCard

### get by id

```python
>>> payment_profile = PaymentProfileCreditCard.get(2771252)
>>> payment_profile
Cliente1 - **** **** **** 4448
```

### get list

```python
>>> payment_profiles = PaymentProfileCreditCard.get_list()
>>> payment_profiles
[Cliente1 - **** **** **** 4448, Cliente2 - **** **** **** 5550]
```

### create

```python
>>> data = {
            'holder_name': 'CLIENTE TESTE',
            'card_expiration': '12/2018',
            'card_number': '4444444444444448',
            'card_cvv': '000',
            'customer_id': self.customer_id
          }
>>> payment_profile = PaymentProfileCreditCard(**data)
>>> payment_profile.save()
>>> payment_profile
Cliente1 - **** **** **** 4448
```

### remove

```python
>>> payment_profile = PaymentProfileCreditCard(000)
>>> payment_profile.remove()
>>> payment_profile.status
inactive
```

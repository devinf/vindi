from .entity import Entity
from .product import Product


class Bill(Entity):
    endpoint = '/bills'
    key_list_name = 'bills'
    key_name = 'bill'
    fields = (
        'id',
        'amount',
        'installments',
        'status',
        'seen_at',
        'billing_at',
        'due_at',
        'url',
        'created_at',
        'updated_at',
        'bill_items',
        'charges'
    )
    required_fields = (
        'customer_id',
        'payment_method_code',
        'id',
        'bill_items'
    )

    def add_bill_item(self, bill_item, quantity):
        if not isinstance(bill_item, Product):
            raise ValueError("'bill_item' must be a Product")

        bill_items = self.bill_items if isinstance(self.bill_items, list) else list()
        bill_items.append({"amount": quantity, "product_id": bill_item.id})

        setattr(self, 'bill_items', bill_items)

    def save(self):
        if not hasattr(self, 'bill_items'):
            raise AttributeError('Bill has no attribute bill_items')
        return super(Bill, self).save()

    def __repr__(self):
        return str(self.amount)

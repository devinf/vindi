from .entity import Entity


class Customer(Entity):
    endpoint = '/customers'
    key_list_name = 'customers'
    key_name = 'customer'
    required_fields = ('name',)
    fields = (
        'id',
        'name',
        'email',
        'registry_code',
        'code',
        'notes',
        'status',
        'created_at',
        'updated_at',
        'metadata',
        'address',
        'phones'
    )

    @classmethod
    def get_archived(cls):
        query = 'status:archived'
        return cls.get_list(query=query)

    @classmethod
    def get_list(cls, query='-status:archived'):
        return super(Customer, cls).get_list(query=query)

    def remove(self):
        if hasattr(self, 'status') and self.status == 'archived':
            raise ValueError('Customer is archived')

        return super(Customer, self).remove()

    def __repr__(self):
        if hasattr(self, 'status') and self.status == 'archived':
            return '{} - {}'.format(self.name, self.status)

        return self.name

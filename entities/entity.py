import os
import requests


class Entity:
    BASE_URL = 'https://app.vindi.com.br/api/v1'
    fields = ()
    required_fields = ()
    key_list_name = None
    key_name = None
    endpoint = ''

    def __init__(self, **data):
        self.__token = os.environ.get('VINDI_TOKEN', None)

        for attribute in self.fields:
            setattr(self, attribute, None)

        for attribute in data:
            setattr(self, attribute, data.get(attribute))

    def save(self):
        if not self.id:
            return self.create()
        else:
            return self.update()

    def create(self):
        self.__is_valid()
        params = self.__request_default_params()
        response = requests.post(
            self.get_url(),
            json=self.__data_to_json(),
            **params
        )

        obj = response.json()

        if response.status_code is 201:
            self.__fill_self(obj)
            return self
        else:
            raise requests.HTTPError(obj)

    @classmethod
    def get(cls, id):
        params = cls.__request_default_params()

        response = requests.get(
            "{url}/{id}".format(url=cls.get_url(), id=id),
            **params
        )

        obj = response.json()

        if response.status_code is 200:
            return cls(**obj[cls.key_name])
        else:
            raise requests.HTTPError(obj)

    @classmethod
    def get_list(cls, query=''):
        params = cls.__request_default_params()
        payload = cls.__request_default_payload(query)

        response = requests.get(
            cls.get_url(),
            params=payload,
            **params
        )

        if response.status_code is 200:
            treated_list = cls.__treat_list(response)
            return treated_list
        else:
            raise requests.HTTPError(response.json())

    def update(self):
        params = self.__request_default_params()
        response = requests.put(
            "{url}/{id}".format(url=self.get_url(), id=self.id),
            data=self.__data_to_json(),
            **params
        )

        obj = response.json()

        if response.status_code is 200:
            self.__fill_self(obj)
            return self
        else:
            raise requests.HTTPError(obj)

    def remove(self):
        if not self.id:
            raise AttributeError('\'id\' is not defined')

        params = self.__request_default_params()
        response = requests.delete(
            "{url}/{id}".format(url=self.get_url(), id=self.id),
            **params
        )

        obj = response.json()

        if response.status_code is 200:
            self.__fill_self(obj)
            return self
        else:
            raise requests.HTTPError(obj)

    @classmethod
    def get_url(cls):
        return "{base_url}{endpoint}".format(base_url=cls.BASE_URL, endpoint=cls.endpoint)

    @staticmethod
    def __request_default_params():
        token = os.environ.get('VINDI_TOKEN', None)
        timeout = int(os.environ.get('VINDI_TIMEOUT', None))

        params = {
            'auth': (token, ''),
            'timeout': timeout
        }

        return params

    @staticmethod
    def __request_default_payload(query):
        items_per_page = int(os.environ.get('VINDI_ITEMS_PER_PAGE', None))

        payload = {
            'per_page': items_per_page,
            'query': query
        }

        return payload

    @classmethod
    def __treat_list(cls, response):
        if not cls.key_list_name:
            raise AttributeError('key_list_name is not defined.')

        response_list = response.json()[cls.key_list_name]

        treated_list = list()

        for obj in response_list:
            obj = cls(**obj)
            treated_list.append(obj)

        return treated_list

    def __fill_self(self, obj):
        if not self.key_name:
            raise AttributeError('key_name is not defined.')

        for attribute in self.fields:
            setattr(self, attribute, obj[self.key_name].get(attribute))

    def to_json(self):
        return self.__data_to_json()

    def __data_to_json(self):
        data = dict()

        for field in self.fields:
            if getattr(self, field, None):
                data[field] = getattr(self, field)

        for field in self.required_fields:
            if getattr(self, field, None):
                data[field] = getattr(self, field)

        return data

    def __is_valid(self):
        for attribute in self.required_fields:
            if not hasattr(self, attribute):
                raise AttributeError('{attribute} is required'.format(attribute=attribute))


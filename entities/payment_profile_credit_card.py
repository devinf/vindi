from .entity import Entity


class PaymentProfileCreditCard(Entity):
    endpoint = '/payment_profiles'
    payment_method_code = 'credit_card'
    required_fields = (
        'holder_name',
        'card_expiration',
        'card_number',
        'card_cvv',
        'customer_id'
    )
    fields = (
        'id',
        'status',
        'holder_name',
        'registry_code',
        'bank_branch',
        'bank_account',
        'card_expiration',
        'card_number_first_six',
        'card_number_last_four',
        'token',
        'created_at',
        'updated_at',
        'payment_company',
        'payment_method',
        'customer'
    )
    key_list_name = 'payment_profiles'
    key_name = 'payment_profile'

    @classmethod
    def get_by_customer_id(cls, id, query='status=active'):
        query += ' type=PaymentProfile::CreditCard customer_id={id}'.format(id=id)

        payment_profiles = super(PaymentProfileCreditCard, cls).get_list(query=query)

        if len(payment_profiles) < 1:
            return None

        return payment_profiles[0]

    @classmethod
    def get_list(cls, query='type=PaymentProfile::CreditCard'):
        return super(PaymentProfileCreditCard, cls).get_list(query=query)

    def remove(self):
        response = super(PaymentProfileCreditCard, self).remove()
        return response.status == 'inactive'

    def __repr__(self):
        return "{self.holder_name} - **** **** **** {self.card_number_last_four}".format(
            self=self
        )

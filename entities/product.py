from .entity import Entity


class Product(Entity):
    endpoint = '/products'
    key_list_name = 'products'
    fields = (
        'id',
        'name',
        'code',
        'unit',
        'status',
        'description',
        'invoice',
        'created_at',
        'pricing_schema',
        'metadata'
    )

    @classmethod
    def get(cls, id):
        products = super(Product, cls).get_list(query='code:{id}'.format(id=id))

        if len(products) > 0:
            return products[0]
        else:
            return None

    def save(self):
        raise NotImplementedError

    def remove(self):
        raise NotImplementedError

    def __repr__(self):
        return self.name

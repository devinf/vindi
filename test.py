import unittest

from entities.bill import Bill
from entities.customer import Customer
from entities.payment_profile_credit_card import PaymentProfileCreditCard
from entities.product import Product

from vindi import Vindi


v = Vindi(token='dshZaVKRrhXB9vTmVHOXT5C_kMobGHfw')


class CustomerTestCase(unittest.TestCase):
    id = None

    def test_create(self):
        customer = Customer(name='unittest')
        customer.save()
        self.__class__.id = customer.id

        self.assertTrue(customer.id)

    def test_get(self):
        customer = Customer.get(self.__class__.id)

        self.assertTrue(customer.id)

    def test_update(self):
        customer = Customer.get(self.__class__.id)
        customer.name = 'Unit Test'
        customer.save()

        self.assertTrue(customer.name == 'Unit Test')

    def test_remove(self):
        customer = Customer.get(self.__class__.id)
        customer.remove()

        self.assertTrue(customer.status == 'archived')


class PaymentProfileCreditCardTestCase(unittest.TestCase):
    id = None
    customer_id = 2714152

    def test_create(self):
        data = {
            'holder_name': 'CLIENTE TESTE',
            'card_expiration': '12/2018',
            'card_number': '4444444444444448',
            'card_cvv': '000',
            'customer_id': self.customer_id
        }
        payment_profile = PaymentProfileCreditCard(**data)
        payment_profile.save()

        self.__class__.id = payment_profile.id

        self.assertTrue(payment_profile.id)

    def test_get(self):
        payment_profile = PaymentProfileCreditCard.get(self.__class__.id)

        self.assertTrue(payment_profile)

    def test_remove(self):
        payment_profile = PaymentProfileCreditCard.get(self.__class__.id)
        payment_profile.remove()

        self.assertTrue(payment_profile.status == 'inactive')


class ProductTestCase(unittest.TestCase):
    def test_get_list(self):
        products = Product.get_list()

        self.assertTrue(products)

    def test_get(self):
        product = Product.get('inflr_credits')

        self.assertTrue(product)


class BillTestCase(unittest.TestCase):
    def test_post(self):
        product = Product.get('inflr_credits')
        bill = Bill(
            payment_method_code='credit_card',
            customer_id='2714152'
        )
        bill.add_bill_item(product, 500)
        bill.save()

        self.assertTrue(bill.id)

    def test_get_list(self):
        bills = Bill.get_list()

        self.assertTrue(bills)

if __name__ == '__main__':
    unittest.main()

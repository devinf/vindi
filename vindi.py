import os


class Vindi:
    BASE_URL = 'https://app.vindi.com.br/api/v1'

    def __init__(self, token, items_per_page=25, timeout=30):
        os.environ['VINDI_TOKEN'] = token
        os.environ['VINDI_ITEMS_PER_PAGE'] = str(items_per_page)
        os.environ['VINDI_TIMEOUT'] = str(timeout)

    def set_items_per_page(self, items_per_page):
        os.environ['VINDI_ITEMS_PER_PAGE'] = items_per_page

    def set_timeout(self, timeout):
        os.environ['VINDI_TIMEOUT'] = timeout
